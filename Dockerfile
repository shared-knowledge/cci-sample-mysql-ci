FROM ubuntu:18.04

WORKDIR /app
COPY credentials /app

# Install mysql client
RUN apt-get update \
    && apt-get install -y --no-install-recommends mysql-client \
    && rm -rf /var/lib/apt/lists/*

# Install wget
RUN apt-get update \
	&& apt-get install -y wget \
	&& rm -rf /var/lib/apt/lists/*

# Install gcloud pre-requisites
RUN apt-get update && apt-get install -y curl
RUN apt-get update && apt-get install -y python-pip
RUN apt-get update && apt-get install -y python
RUN apt-get update && apt-get install -y python3.6

# Downloading gcloud package
RUN curl -L https://dl.google.com/dl/cloudsdk/release/google-cloud-sdk.tar.gz > /tmp/google-cloud-sdk.tar.gz

# Installing the gcloud package
RUN mkdir -p /usr/local/gcloud \
    && tar -C /usr/local/gcloud -xvf /tmp/google-cloud-sdk.tar.gz \
    && /usr/local/gcloud/google-cloud-sdk/install.sh

# Adding the package path to local
ENV PATH $PATH:/usr/local/gcloud/google-cloud-sdk/bin

# Install cloud Sql proxy 
RUN wget https://dl.google.com/cloudsql/cloud_sql_proxy.linux.amd64 -O cloud_sql_proxy

# Make sql proxy executable
RUN chmod +x cloud_sql_proxy

# Start the Proxy
ENTRYPOINT ./cloud_sql_proxy -instances=shared-knowledge-2019:us-central1:sharedknowledgedbwip=tcp:0.0.0.0:3306 -credential_file=./cci-sample-builduser-wip-for-gcp-cloud-sql.json